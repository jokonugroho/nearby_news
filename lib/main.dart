import 'package:flutter/material.dart';
import 'package:nearby_news/models/custom_color.dart';
import 'package:nearby_news/view/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  custom_color ccolor = custom_color();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(title: "Nearby",),
      theme: ccolor.themeData,
    );
  }
}
