import 'package:flutter/material.dart';

class custom_color{
  ThemeData get themeData {
    final ThemeData base = ThemeData.light();
    return base.copyWith(
      colorScheme: _shrineColorScheme,
      accentColor: shrinePink100,
      primaryColor: shrinePink400,
      buttonColor: shrinePink400,
      scaffoldBackgroundColor: shrineBackgroundWhite,
      cardColor: shrineBackgroundWhite,
      textSelectionColor: shrinePink400,
      errorColor: shrineErrorRed,
      buttonTheme: const ButtonThemeData(
        colorScheme: _shrineColorScheme,
        textTheme: ButtonTextTheme.normal,
      ),
      primaryIconTheme: _customIconTheme(base.iconTheme),
      textTheme: _buildShrineTextTheme(base.textTheme),
      primaryTextTheme: _buildShrineTextTheme(base.primaryTextTheme),
      accentTextTheme: _buildShrineTextTheme(base.accentTextTheme),
      iconTheme: _customIconTheme(base.iconTheme),
    );
  }

  IconThemeData _customIconTheme(IconThemeData original) {
    return original.copyWith(color: shrinePink50);
  }

  TextTheme _buildShrineTextTheme(TextTheme base) {
    return base
        .copyWith(
      caption: base.caption.copyWith(
        fontWeight: FontWeight.w400,
        fontSize: 14,
        letterSpacing: defaultLetterSpacing,
      ),
      button: base.button.copyWith(
        fontWeight: FontWeight.w500,
        fontSize: 14,
        letterSpacing: defaultLetterSpacing,
      ),
    )
        .apply(
      fontFamily: 'Rubik',
      displayColor: shrinePink50,
      bodyColor: Colors.black54,
    );
  }

}


const ColorScheme _shrineColorScheme = ColorScheme(
  primary: shrinePink400,
  primaryVariant: shrinePink300,
  secondary: shrinePink50,
  secondaryVariant: shrinePink50,
  surface: shrineSurfaceWhite,
  background: shrineBackgroundWhite,
  error: shrineErrorRed,
  onPrimary: shrinePink50,
  onSecondary: shrinePink50,
  onSurface: shrinePink50,
  onBackground: shrinePink50,
  onError: shrineSurfaceWhite,
  brightness: Brightness.light,
);

const Color shrinePink50 = Color(0xFFC6B6EE);
const Color shrinePink100 = Color(0xFF875EF4);
const Color shrinePink300 = Color(0xFF7649EF);
const Color shrinePink400 = Color(0xFF7041EE);

const Color shrineErrorRed = Color(0xFFC5032B);

const Color shrineSurfaceWhite = Color(0xFFFFFBFA);
const Color shrineBackgroundWhite = Colors.white;

const defaultLetterSpacing = 0.03;