import 'package:nearby_news/models/user.dart';
import 'package:flutter/material.dart';

const List<News> news = const <News>[
  const News(
      title: "Potret Keseruan Konvoi Motor Listrik Keliling Bandung",
      description:"Pemerintah Provinsi (Pemprov) Jawa Barat melakukan test drive sepeda motor listrik. Test drive ini dilakukan dari Kawasan Gedung Sate ke Sukajadi, Kota Bandung.",
      image:"https://akcdn.detik.net.id/community/media/visual/2020/11/28/potret-keseruan-konvoi-motor-listrik-keliling-bandung-7_43.jpeg",
      publish_dtm: "Minggu, 29 Nov 2020 06:23 WIB"
  ),
  const News(
      title: "Bandung Punya Kafe Tropis Bergaya Pantai",
      description:"Bandung merupakan salah satu kota dengan berbagai jenis kafe yang berkonsep unik dan menarik. Nah, kafe ini memiliki atmosfer pantai yang khas di daerah tropis.",
      image:"https://akcdn.detik.net.id/community/media/visual/2020/11/28/kafe-tropical-vibes-bandung-2_43.jpeg",
      publish_dtm: "Sabtu, 28 Nov 2020 14:17 WIB"
  ),
  const News(
      title: "20 Massa HRS Positif Corona di Bogor, Ridwan Kamil: Tes Terus Dilakukan",
      description:"Pelacakan kontak erat kepada peserta kegiatan peletakan batu pertama masjid di Pondok Pesantren Agrokultural Markaz Syariat terus dilakukan.",
      image:"https://akcdn.detik.net.id/community/media/visual/2020/07/24/gubernur-jabar-ridwan-kamil-menyoroti-kasus-pemerkosaan-gadis-di-cianjur_43.jpeg",
      publish_dtm: "Sabtu, 28 Nov 2020 14:17 WIB"
  ),
  const News(
      title: "Eksistensi TAP Gubernur Jawa Barat Ridwan Kamil Kembali Disorot",
      description:"Eksistensi Tim Akselerasi Pembangunan (TAP) Jawa Barat disoal sekelompok masyarakat yang tergabung dalam Pembela Kesatuan Tanah Air Indonesia Bersatu.",
      image:"https://akcdn.detik.net.id/community/media/visual/2020/08/17/gubernur-jabar-ridwan-kamil_43.jpeg",
      publish_dtm: "Jumat, 27 Nov 2020 18:51 WIB"
  ),
  const News(
      title: "Polisi Akan Panggil Para Saksi soal Kerumunan HRS di Bogor Pekan Depan",
      description:"\"Surat pemberitahuan dimulainya penyidikan (SPDP) tadi sudah dikirim ke kejaksaan. Rencana pemeriksaan saksi, dimulai tanggal 1 Desember (2020),\"",
      image:"https://akcdn.detik.net.id/community/media/visual/2020/11/13/habib-rizieq-disambut-massa-di-simpang-gadog-jelang-ceramah-di-megamendung-bogor-1_43.jpeg?w=250&q=",
      publish_dtm: "Jumat, 27 Nov 2020 17:53 WIB"
  ),
  const News(
      title: "Pemkot Bandung Imbau Warga Tak Gelar Resepsi Pernikahan Besar-besaran",
      description:"Pemerintah Kota Bandung mengimbau warga agar tak menggelar resepsi pernikahan secara besar-besaran. Hal itu demi mencegah terjadinya kerumunan.",
      image:"https://akcdn.detik.net.id/community/media/visual/2020/11/02/ilustrasi-virus-corona-covid-19-10_43.jpeg?w=250&q=",
      publish_dtm: "Jumat, 27 Nov 2020 15:44 WIB"
  ),
  const News(
      title: "Pilkada Serentak 2020, Ridwan Kamil Berharap Partisipasi Warga Tinggi",
      description:"Gubernur Jawa Barat Ridwan Kamil mengharapkan partisipasi warga tetap tinggi dalam Pilkada Serentak 2020 meski di tengah pandemi COVID-19.",
      image:"https://akcdn.detik.net.id/community/media/visual/2020/06/06/gubernur-jabar-ridwan-kamil_43.jpeg?w=250&q=",
      publish_dtm: "Jumat, 27 Nov 2020 15:17 WIB"
  ),
  const News(
      title: "Mayat Janin Bayi Perempuan Ditemukan Menggantung di Pintu Masjid Bandung",
      description:"Sosok mayat janin bayi ditemukan di sebuah masjid di Bandung. Janin bayi jenis kelamin perempuan ditemukan di dalam kantong kresek menggantung di pintu masjid.",
      image:"https://akcdn.detik.net.id/community/media/visual/2017/07/07/04d6bdb2-2cac-4687-93cd-130c776b3edd_43.jpg?w=250&q=",
      publish_dtm: "Jumat, 27 Nov 2020 14:17 WIB"
  ),
  const News(
      title: "Ini Respons PDIP Jabar soal Walkot Cimahi Ajay M Priatna Di-OTT KPK",
      description:"Wali Kota Cimahi Ajay Priatna ditangkap KPK berkaitan kasus suap. PDIP Jawa Barat angkat suara terkait Operasi Tangkap Tangan (OTT) KPK terhadap kadernya itu.",
      image:"https://akcdn.detik.net.id/community/media/visual/2020/11/27/wali-kota-cimahi-ajay-priatna-1_43.jpeg?w=250&q=",
      publish_dtm: "Jumat, 27 Nov 2020 13:52 WIB"
  ),
  const News(
      title: "Dirawat di RSCM, Abu Bakar Ba'asyir Alami Sakit Komplikasi",
      description:"Terpidana kasus terorisme Abu Bakar Ba'asyir dirawat di Rumah Sakit Cipto Mangunkusumo. Ba'asyir disebut mengalami sakit komplikasi.",
      image:"https://akcdn.detik.net.id/community/media/visual/2016/01/18/04f5f45c-15b2-48bf-bdc3-d8206c37336f_43.jpg?w=250&q=",
      publish_dtm: "Jumat, 27 Nov 2020 13:23 WIB"
  )
];
class News {
  const News({this.title, this.description, this.image, this.publish_dtm});
  final String title;
  final String description;
  final String image;
  final String publish_dtm;
}

class NewsCard extends StatelessWidget {
  const NewsCard(
      {Key key,
        this.news,
        this.onTap,
        @required this.item,
        this.selected: false})
      : super(key: key);

  final News news;
  final VoidCallback onTap;
  final News item;
  final bool selected;
  @override
  Widget build(BuildContext context) {

    TextStyle textStyle = Theme.of(context).textTheme.display1;
    if (selected) {
      textStyle.copyWith(color: Colors.lightGreenAccent[400]);
    }
    return Card(
        color: Colors.white,
        child: InkWell(
          onTap: onTap,
          child: Row(
            children: <Widget>[
              new Container(
                padding: const EdgeInsets.all(8.0),
                alignment: Alignment.topLeft,
                child: Image.network(
                  news.image,
                  height: 120,
                  width: 120,
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    new Container(
                      padding: const EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        news.title,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        ),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                      ),
                    ),
                    new Container(
                      padding: const EdgeInsets.all(10.0),
                      alignment: Alignment.topLeft,
                      child: Text(
                        news.description,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w300,
                        ),
                        textAlign: TextAlign.left,
                        maxLines: 5,
                      ),
                    )
                  ],
                ),
              )
            ],
            crossAxisAlignment: CrossAxisAlignment.start,
          ),
        )
    );
  }
}