import 'package:flutter/material.dart';
import 'package:nearby_news/models/custom_color.dart';
import 'package:nearby_news/view/home.dart';
import 'package:nearby_news/view/profile.dart';
import 'package:nearby_news/view/login.dart';
class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Nearby News')),
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30.0),
              bottomRight: Radius.circular(30.0),
            ),
          ),
          child: Container(
              decoration: BoxDecoration(
                color: Theme.of(context).scaffoldBackgroundColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0),
                ),
              ),
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(15.0),
                      child: Text("Aplikasi Nearby News adalah aplikasi portal berita yang menyajikan berita-berita yang sesuai dengan minat baca dan di urutkan berdasarkan lokasi berita terdekat.\n\nFitur yang ada pada aplikasi ini selain login dan register adalah membaca berata, mencari berita, mengomentari, memberi rating terhadap berita dan membookmark berita.\n\nDan untuk alur proses dari aplikasi ini yaitu membaca berita dan memberikan tanggapan terhadap berita tersebut baik melalui komentar atau pun memberikan rating yang bisa membantu pembaca lain agar tidak terjebak berita hoax",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                            fontWeight: FontWeight.w600
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        children: [
                          Center(
                            child: ListTile(
                              leading: Icon(Icons.copyright),
                              title: Text("Joko Nugroho - 18111205", style: TextStyle(color: Colors.grey),),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
          ),
        )
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: [
                  Center(
                    child: CircleAvatar(
                      radius: 50,
                      backgroundImage: NetworkImage("https://git.neuron.id/assets/no_avatar-849f9c04a3a0d0cea2424ae97b27447dc64a7dbfae83c036c45b403392f0e8ba.png"),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 15),
                    child: Text(
                      "Joko Nugroho",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: Colors.white
                      ),
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                color: shrinePink400,
              ),
            ),
            ListTile(
              title: Text('Profile'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  Profile()));
              },
            ),
            ListTile(
              title: Text('News'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (conetxt) =>  MyHomePage()));
              },
            ),
            ListTile(
              title: Text('About'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  About()));
              },
            ),
            ListTile(
              title: Text('Login'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  Login()));
              },
            ),
          ],
        ),
      ),
    );
  }
}