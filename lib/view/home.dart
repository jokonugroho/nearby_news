import 'package:flutter/material.dart';
import 'package:nearby_news/models/custom_color.dart';
import 'package:nearby_news/view/about.dart';
import 'package:nearby_news/view/profile.dart';
import 'package:nearby_news/view/detail_news.dart';
import 'package:nearby_news/models/news.dart';
import 'package:nearby_news/view/login.dart';

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedDestination = 0;
  int _currentIndex = 0;
  final _listPage = <Widget>[
    ListView.builder(
      itemCount: news.length,
      itemBuilder: (context, index) {
        return NewsCard(
          news: news[index],
          item: news[index],
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => NewsDetail(news: news[index]))
            );
          },
        );
      },
    ),
  ];
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Nearby News'),
      ),
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30.0),
              bottomRight: Radius.circular(30.0),
            ),
          ),
          child: Container(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            decoration: BoxDecoration(
              color: Theme.of(context).scaffoldBackgroundColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
            ),
            child: _listPage[_currentIndex],
          )
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: [
                  Center(
                    child: CircleAvatar(
                      radius: 50,
                      backgroundImage: NetworkImage("https://git.neuron.id/assets/no_avatar-849f9c04a3a0d0cea2424ae97b27447dc64a7dbfae83c036c45b403392f0e8ba.png"),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 15),
                    child: Text(
                      "Joko Nugroho",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: Colors.white
                      ),
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                color: shrinePink400,
              ),
            ),
            ListTile(
              title: Text('Profile'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  Profile()));
              },
            ),
            ListTile(
              title: Text('News'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (conetxt) =>  MyHomePage()));
              },
            ),
            ListTile(
              title: Text('About'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  About()));
              },
            ),
            ListTile(
              title: Text('Login'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  Login()));
              },
            ),
          ],
        ),
      ),
    );
  }

  void selectDestination(int index) {
    setState(() {
      _selectedDestination = index;
    });
  }
}