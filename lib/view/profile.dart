import 'package:flutter/material.dart';
import 'package:nearby_news/models/custom_color.dart';
import 'package:nearby_news/view/about.dart';
import 'package:nearby_news/view/home.dart';
import 'package:nearby_news/view/login.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nearby News'),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: [
                  Center(
                    child: CircleAvatar(
                      radius: 50,
                      backgroundImage: NetworkImage("https://git.neuron.id/assets/no_avatar-849f9c04a3a0d0cea2424ae97b27447dc64a7dbfae83c036c45b403392f0e8ba.png"),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 15),
                    child: Text(
                      "Joko Nugroho",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: Colors.white
                      ),
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                color: shrinePink400,
              ),
            ),
            ListTile(
              title: Text('Profile'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  Profile()));
              },
            ),
            ListTile(
              title: Text('News'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (conetxt) =>  MyHomePage()));
              },
            ),
            ListTile(
              title: Text('About'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  About()));
              },
            ),
            ListTile(
              title: Text('Login'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  Login()));
              },
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30.0),
              bottomRight: Radius.circular(30.0),
            ),
          ),
          child: Container(
            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
            decoration: BoxDecoration(
              color: Theme.of(context).scaffoldBackgroundColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
            ),
            child: Container(
              padding: EdgeInsets.fromLTRB(15, 35, 15, 25),
              child: Column(
                children: [
                  Center(
                    child: CircleAvatar(
                      radius: 55,
                      backgroundColor: shrinePink50,
                      child: CircleAvatar(
                          radius: 50,
                          backgroundImage: NetworkImage("https://git.neuron.id/assets/no_avatar-849f9c04a3a0d0cea2424ae97b27447dc64a7dbfae83c036c45b403392f0e8ba.png")
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(30, 15, 30, 15),
                      child: TextFormField(
                        initialValue: "koynugroho",
                        cursorColor: Theme.of(context).cursorColor,
                        decoration: InputDecoration(
                            icon: Icon(Icons.perm_identity), labelText: "Username"),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(30, 15, 30, 15),
                      child: TextFormField(
                        initialValue: "Joko Nugroho",
                        cursorColor: Theme.of(context).cursorColor,
                        decoration: InputDecoration(
                            icon: Icon(Icons.perm_identity), labelText: "Nama Lengkap"),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(30, 15, 30, 15),
                      child: TextFormField(
                        initialValue: "jokonugroho150@gmail.com",
                        cursorColor: Theme.of(context).cursorColor,
                        decoration: InputDecoration(
                            icon: Icon(Icons.email), labelText: "E-mail"),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(30, 15, 30, 15),
                      child: TextFormField(
                        obscureText: true,
                        initialValue: "sunduluatuh",
                        cursorColor: Theme.of(context).cursorColor,
                        decoration: InputDecoration(
                            icon: Icon(Icons.lock), labelText: "Password"),
                      ),
                    ),
                  ),
                  Center(
                    child: Container(
                      child: FlatButton.icon(
                        onPressed: () {

                        },
                        icon: Icon(Icons.save),
                        label: Text("Simpan"),
                        textColor: Colors.white,
                        color: Theme.of(context).primaryColor,
                        minWidth: 350,
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        )
      ),
    );
  }
}